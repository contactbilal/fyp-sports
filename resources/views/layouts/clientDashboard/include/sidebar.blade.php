<div class="profile-sidebar shadow">
                    <!-- SIDEBAR USERPIC -->
                    <div class="profile-userpic text-center">
                        <img src="https://gravatar.com/avatar/31b64e4876d603ce78e04102c67d6144?s=80&d=https://codepen.io/assets/avatars/user-avatar-80x80-bdcd44a3bfb9a5fd01eb8b86f9e033fa1a9897c3a15b33adfc2649a002dab1b6.png" class="mx-auto" alt="">
                    </div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name">
                            {{auth()->user()->name ?? ''}}
                        </div>
                    </div>
                    <!-- END SIDEBAR USER TITLE -->
                    <!-- SIDEBAR BUTTONS -->
                    <div class="profile-userbuttons">

                        <a href="{{ route('userdashboard.editUserInfo.edit',auth()->user()->id) }}" class="btn btn-success btn-sm">Edit Info</a>


                        <!-- <button type="button" class="btn btn-danger btn-sm">Settings</button> -->
                    </div>
                    <!-- END SIDEBAR BUTTONS -->
                    <!-- SIDEBAR MENU -->
                    <div class="profile-usermenu">
                        <ul class="nav">
                            <li class="active w-100">
                                <a href="#" class="w-100">
                                    <i class="glyphicon glyphicon-home"></i> Overview
                                </a>
                            </li>
                            <li class="  w-100">
                                <a href="#" class="w-100">
                                    <i class="glyphicon glyphicon-home"></i> Tickets
                                </a>
                            </li>
                            <li class="  w-100">
                                <a href="#" class="w-100">
                                    <i class="glyphicon glyphicon-home"></i> Event
                                </a>
                            </li>
                            <li class="w-100">
                                <a href="#" class="w-100">
                                    <i class="glyphicon glyphicon-user"></i> Account Settings
                                </a>
                            </li>
                            <!-- <li class="w-100">
                                <a href="#" target="_blank">
                                    <i class="glyphicon glyphicon-ok"></i> Tasks </a>
                            </li>
                            <li class="w-100">
                                <a href="#">
                                    <i class="glyphicon glyphicon-flag"></i> Help </a>
                            </li> -->
                        </ul>
                    </div>
                    <!-- END MENU -->

                    <div class="portlet light bordered">
                        <!-- STAT -->
                        <!-- <div class="row list-separated profile-stat">
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <div class="uppercase profile-stat-title"> 37 </div>
                                <div class="uppercase profile-stat-text"> Projects </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <div class="uppercase profile-stat-title"> 51 </div>
                                <div class="uppercase profile-stat-text"> Tasks </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <div class="uppercase profile-stat-title"> 61 </div>
                                <div class="uppercase profile-stat-text"> Uploads </div>
                            </div>
                        </div> -->
                        <!-- END STAT -->
                        <div>
                            <!-- <h4 class="profile-desc-title">About Jason Davis</h4>
                            <span class="profile-desc-text"> Lorem ipsum dolor sit amet diam nonummy nibh dolore. </span> -->
                            <div class="margin-top-20 d-flex align-items-center profile-desc-link">
                                <i class="fa fa-envelope"></i>
                                <a href=" ">{{Auth::user()->email ?? ''}}
                                </a>
                            </div>
                            <div class="margin-top-20 profile-desc-link">
                                <i class="fa fa-phone"></i>
                                <a href=" ">{{Auth::user()->phone ?? ''}}</a>
                            </div>

                        </div>
                    </div>
                </div>
