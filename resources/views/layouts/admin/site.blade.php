<!DOCTYPE html>

<html lang="en" class="default-style">

<head>
    <title>Admin Dashboard</title>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="icon" type="image/x-icon" href="favicon.ico">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900" rel="stylesheet">

    <!-- Icon fonts -->
    <link rel="stylesheet" href="{{asset('admin/assets/vendor/fonts/fontawesome.css')}}">
    <link rel="stylesheet" href="{{ asset('admin/assets/vendor/fonts/ionicons.css') }}">
    <link rel="stylesheet" href="{{ asset("admin/assets/vendor/fonts/linearicons.css") }}">
    <link rel="stylesheet" href="{{ asset("admin/assets/vendor/fonts/open-iconic.css") }}">
    <link rel="stylesheet" href="{{ asset("admin/assets/vendor/fonts/pe-icon-7-stroke.css") }}">

    <!-- Core stylesheets -->
    <link rel="stylesheet" href="{{ asset("admin/assets/vendor/css/rtl/bootstrap.css") }}" class="theme-settings-bootstrap-css">
    <link rel="stylesheet" href="{{ asset("admin/assets/vendor/css/rtl/appwork.css") }}" class="theme-settings-appwork-css">
    <link rel="stylesheet" href="{{ asset("admin/assets/vendor/css/rtl/theme-corporate.css") }}" class="theme-settings-theme-css">
    <link rel="stylesheet" href="{{ asset("admin/assets/vendor/css/rtl/colors.css") }}" class="theme-settings-colors-css">
    <link rel="stylesheet" href="{{ asset("admin/assets/vendor/css/rtl/uikit.css") }}">
    <link rel="stylesheet" href="{{ asset("admin/assets/css/demo.css") }}">

    <!-- Load polyfills -->
    <script src="{{ asset("admin/assets/vendor/js/polyfills.js") }}"></script>
    <script>
        document['documentMode'] === 10 && document.write('<script src="https://polyfill.io/v3/polyfill.min.js?features=Intl.~locale.en"><\/script>')
    </script>

    <script src="{{ asset("admin/assets/vendor/js/material-ripple.js") }}"></script>
    <script src="{{ asset("admin/assets/vendor/js/layout-helpers.js") }}"></script>

    <!-- Theme settings -->
    <!-- This file MUST be included after core stylesheets and layout-helpers.js in the <head> section -->
    <!-- Core scripts -->
    <script src="assets/vendor/js/pace.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- Libs -->
    <link rel="stylesheet" href="assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css">

</head>

<body>
    <div class="page-loader">
        <div class="bg-primary"></div>
    </div>

    <!-- Layout wrapper -->
    <div class="layout-wrapper layout-2">
        <div class="layout-inner">

            <!-- Layout sidenav -->
            <div id="layout-sidenav" class="layout-sidenav sidenav sidenav-vertical bg-dark">
                @include("layouts.admin.include.sideNavbar")
            </div>
            <!-- / Layout sidenav -->


            <!-- Layout container -->
            <div class="layout-container">


            <!-- ============= Layout navbar ===========-->
             @include("layouts.admin.include.topHeader")
            <!-- ============/ Layout navbar =============-->

                <!-- Layout content -->
                <div class="layout-content">

                    <!-- Content -->
                    <div class="container-fluid flex-grow-1 container-p-y">

                       @yield('content')

                    </div>
                    <!-- / Content -->

                    <!-- Layout footer -->
                    @include('layouts.admin.include.footer')
                    <!-- / Layout footer -->

                </div>
                <!-- Layout content -->

            </div>
            <!-- / Layout container -->

        </div>

        <!-- Overlay -->
        <div class="layout-overlay layout-sidenav-toggle"></div>
    </div>
    <!-- / Layout wrapper -->

    <!-- Core scripts -->
    <script src="{{ asset("admin/assets/vendor/libs/popper/popper.js") }}"></script>
    <script src="{{ asset("admin/assets/vendor/js/bootstrap.js") }}"></script>
    <script src="{{ asset("admin/assets/vendor/js/sidenav.js") }}"></script>

    <!-- Libs -->
    <script src="{{ asset("admin/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js") }}"></script>
    <script src="{{ asset("admin/assets/vendor/libs/chartjs/chartjs.js") }}"></script>

    <!-- Demo -->
    <script src="{{ asset("admin/assets/js/demo.js") }}"></script>
    <script src="{{ asset("admin/assets/js/dashboards_dashboard-1.js") }}"></script>
</body>

</html>
