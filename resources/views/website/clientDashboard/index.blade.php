@extends('layouts.website.site')
@section('content')
<div class="page-heading page-title-bg header-text" style="height: 200px!important;padding:unset!important">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-content text-left">
                   <strong  class="text-white" style="    position: absolute;
                   top: 129px;
                   font-size: 28px;">
                       Dashboard
                   </strong>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="mt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
               @include('layouts.clientDashboard.include.sidebar');
            </div>
            <div class="col-md-9 shadow">
                <div class="profile-content text-center">
                    <div class="signin-image">
                        <figure><img src="{{ asset('assets/images/unnamed.png') }}" alt="sing up image"></figure>
                        {{-- <a href="#" class="signup-image-link">Create an account</a> --}}
                    </div>
                   <h1>
                       Welcome User Dashboard
                   </h1>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
