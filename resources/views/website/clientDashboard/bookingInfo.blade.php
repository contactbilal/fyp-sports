@extends('layouts.website.site')
@section('content')
<div class="page-heading page-title-bg header-text" style="height: 200px!important;padding:unset!important">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-content text-left">
                   <strong  class="text-white" style="    position: absolute;
                   top: 129px;
                   font-size: 28px;">
                       Dashboard / Edit
                   </strong>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="mt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @include('layouts.clientDashboard.include.sidebar');
            </div>
            <div class="col-md-9 shadow">
                <div class="profile-content Edit Details">
                   <h4>
                       Bookings
                   </h4>

                    <div class="card-body">

                    <hr class="border-light m-0">
                    <div class="card-body pb-2">

                        <div class="table-responsive">
                            <table class="table table-hover table-nowrap">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope=" ">Event Name</th>
                                        <th scope=" ">Ticket No</th>
                                        <th scope=" ">Seat No</th>
                                        <th scope=" ">Price</th>
                                        <th scope=" ">status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td data-label="profile">
                                           xyz
                                        </td>
                                        <td data-label="city">
                                            <span>xyz</span>
                                        </td>
                                        <td data-label="Phone">
                                            <a class="text-current" href="#">xyz</a>
                                        </td>
                                        <td data-label="Lead Score">
                                            <a class="text-current" href="tel:202-555-0152">xyz</a>
                                        </td>
                                        <td data-label="Lead Score">
                                            <a class="text-current" href="tel:202-555-0152">xyz</a>
                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
</section>
@endsection
