@extends('layouts.website.site')
@section('content')
<div class="page-heading page-title-bg header-text" style="height: 200px!important;padding:unset!important">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-content text-left">
                   <strong  class="text-white" style="    position: absolute;
                   top: 129px;
                   font-size: 28px;">
                       Dashboard / Edit
                   </strong>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="mt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @include('layouts.clientDashboard.include.sidebar');
            </div>
            <div class="col-md-9 shadow">
                <div class="profile-content Edit Details">
                   <h4>
                       Edit
                   </h4>
                   <form method="post" action="{{route('userdashboard.editUserInfo.update', auth()->user()->id)}}">
                    @csrf
                    @method('put')

                    <div class="card-body">
                         @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                        {{--  <div class="media align-items-center">
                            <img src="assets/img/avatars/5-small.png" alt="" class="d-block ui-w-80">
                            <div class="media-body ml-3">
                                <label class="form-label d-block mb-2">Avatar</label>
                                <label class="btn btn-outline-primary btn-sm">
          Change
          <input type="file" class="user-edit-fileinput">
        </label>&nbsp;
                                <button type="button" class="btn btn-default btn-sm md-btn-flat">Reset</button>
                            </div>
                        </div>

                    </div>  --}}
                    <hr class="border-light m-0">
                    <div class="card-body pb-2">

                        <div class="form-group">
                            <label class="form-label">Username</label>
                            <input type="text" class="form-control mb-1" name="user_name" placeholder="User Name" value="{{ auth()->user()->user_name }}" >
                        </div>

                        <div class="form-group">
                            <label class="form-label">Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Name" value="{{ auth()->user()->name }}" >
                        </div>
                        <div class="form-group">
                            <label class="form-label">E-mail</label>
                            <input type="text" class="form-control mb-1" name="email" placeholder="example@exe.com" value="{{ auth()->user()->email }}" >
                            {{-- <a href="javascript:void(0)" class="small">Resend confirmation</a> --}}
                        </div>
                        <div class="form-group">
                            <label class="form-label">Phone</label>
                            <input type="text" class="form-control" name="phone" placeholder="Phone" value="{{ auth()->user()->phone }}"  >
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-label">Country</label>
                                    <input type="text" class="form-control" name="country" placeholder="Country"   value="{{ auth()->user()->country }}" >
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-label">City</label>
                                    <input type="text" class="form-control" name="city" placeholder="City" value="{{ auth()->user()->city }}" >
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-label">State</label>
                                    <input type="text" class="form-control" name="state" placeholder="state" value="{{ auth()->user()->state }}" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-right m-3 px-3">
                        <button type="submit" class="btn btn-primary">Save changes</button>&nbsp;
                        {{--  <a href="{{ route('users.index') }}" class="btn btn-default">Back</a>  --}}
                    </div>
                </form>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
