@extends('layouts.website.site')
@section('content')
    <!-- Page Content -->
    <div class="page-heading page-title-bg header-text">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-content text-left">
                        <h2>All Sports Event</h2>
                        <h4>Get Tickets to enjoy the event</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="evnetlist">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="filters">
                        <ul>
                            <li class="active" data-filter="*">All </li>
                            @isset($allCategories)
                                @foreach ($allCategories as $item)
                                    <li data-filter=".{{ $item->slug ?? '' }}">{{ $item->category_name ?? '' }}</li>
                                @endforeach
                            @endisset
                        </ul>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="filters-content">
                        <div class="row grid">

                            @isset($events)
                                @foreach ($events as $event)
                                    <div class="col-lg-4 col-md-4 all {{ $category->slug ?? '' }}">
                                        <div class="product-item"> 
                                            <a href="{{ route('event.detail.page', $event->id) }}"><img src="{{ asset('/storage/events/'. $event->avtar) ?? '' }}" class="img-fluid" alt=""></a>
                                            <div class="down-content"> 
                                                <a href="{{ route('event.detail.page', $event->id) }}">
                                                    <h4 class="text-red">{{ $event->event_name ?? '' }}</h4>

                                                </a>
                                                <h5 class="text-muted f-14">{{ $event->date ?? '' }}</h5>
                                                <p>{{ $event->description ?? '' }}</p>
                                                <ul class="star">
                                                    <li>
                                                        <a href="{{ route('event.detail.page', $event->id) }}" class="filled-button">View Details</a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="filled-button">Buy Tickets</a>
                                                    </li>
                                                </ul>

                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endisset

                            
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    {{ $events->links() ?? '' }}
                </div>
            </div>
        </div>
    </div>
@endsection