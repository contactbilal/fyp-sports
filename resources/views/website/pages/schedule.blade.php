@extends('layouts.website.site')
@section('content')

    <!-- Page Content -->
    <div class="page-heading page-title-bg header-text">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-content text-left">
                        <h2>Sports Event Schedule</h2>
                        <h4>Check event organize schedule</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Banner Ends Here -->
    <!-- Events Details bar Start here -->
    <div id="event-details-bar">
        <div class="container-fluid bg-default-theme ">
            <div class="event-bar">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="d-flex py-5 justify-content-center">
                            <div class="d-inline-block  py-3 px-5 text-center">
                                <h5 class="text-white py-3">10</h5>
                                <h4 class="text-white text-uppercase font-ag-bold">Latest Event</h4>
                            </div>
                            <div class="d-inline-block border-left py-3 px-5 text-center">
                                <h5 class="text-white py-3">10</h5>
                                <h4 class="text-white text-uppercase font-ag-bold">Done Event</h4>
                            </div>
                            <div class="d-inline-block border-left py-3 px-5 text-center">
                                <h5 class="text-white py-3">1</h5>
                                <h4 class="text-white text-uppercase font-ag-bold">Upcomming Event</h4>
                            </div>
                            <div class="d-inline-block border-left py-3 px-5 text-center">
                                <h5 class="text-white py-3">10</h5>
                                <h4 class="text-white text-uppercase font-ag-bold">Active Event</h4>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- About Event -->
    <div class="best-features about-features">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h2>Our Background</h2>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="left-content">
                        <h4>Who we are &amp; What we do?</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed voluptate nihil eum consectetur similique? Consectetur, quod, incidunt, harum nisi dolores delectus reprehenderit voluptatem perferendis dicta dolorem non blanditiis
                            ex fugiat. Lorem ipsum dolor sit amet, consectetur adipisicing elit.<br><br>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, consequuntur, modi mollitia corporis ipsa voluptate corrupti eum ratione ex ea praesentium
                            quibusdam? Aut, in eum facere corrupti necessitatibus perspiciatis quis.</p>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="right-image">
                        <img src="assets/images/unnamed.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ======= Schedule Section ======= -->
    <section id="schedule" class="section-with-bg mt-5">
        <div class="container" data-aos="fade-up">
            <div class="section-heading">
                <h2 class="text-white">Event Schedule</h2>
            </div>

            <ul class="nav nav-tabs" role="tablist" data-aos="fade-up" data-aos-delay="100">
                <li class="nav-item">
                    <a class="nav-link active" href="#day-1" role="tab" data-toggle="tab">UFC</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#day-2" role="tab" data-toggle="tab">Cricket</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#day-3" role="tab" data-toggle="tab">Football</a>
                </li>
            </ul>

            <h3 class="sub-heading text-white">Voluptatem nulla veniam soluta et corrupti consequatur neque eveniet officia. Eius necessitatibus voluptatem quis labore perspiciatis quia.</h3>

            <div class="tab-content row justify-content-center" data-aos="fade-up" data-aos-delay="200">

                <!-- Schdule Day 1 -->
                <div role="tabpanel" class="col-lg-9 tab-pane fade show active" id="day-1">
                    <div class="row schedule-item">
                        <div class="col-md-2"><time>10:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>
                        <div class="col-md-10">
                            <div class="speaker">
                                <img src="assets/images/product_01.jpg" alt="Brenden Legros">
                            </div>
                            <h4>Keynote <span>Brenden Legros</span></h4>
                            <p>Facere provident incidunt quos voluptas.</p>
                            <a href="event.html" class="filled-button rounded-0">View Details</a>
                        </div>
                    </div>

                    <div class="row schedule-item">
                        <div class="col-md-2"><time>11:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>

                        <div class="col-md-10">
                            <div class="speaker">
                                <img src="assets/images/product_02.jpg" alt="Hubert Hirthe">
                            </div>
                            <h4>Et voluptatem iusto dicta nobis. <span>Hubert Hirthe</span></h4>
                            <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p>
                            <a href="event.html" class="filled-button rounded-0">View Details</a>
                        </div>
                    </div>

                    <div class="row schedule-item">
                        <div class="col-md-2"><time>12:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>

                        <div class="col-md-10">
                            <div class="speaker">
                                <img src="assets/images/product_03.jpg" alt="Cole Emmerich">
                            </div>
                            <h4>Explicabo et rerum quis et ut ea. <span>Cole Emmerich</span></h4>
                            <p>Veniam accusantium laborum nihil eos eaque accusantium aspernatur.</p>
                            <a href="event.html" class="filled-button rounded-0">View Details</a>
                        </div>
                    </div>

                    <div class="row schedule-item">
                        <div class="col-md-2"><time>10:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>
                        <div class="col-md-10">
                            <div class="speaker">
                                <img src="assets/images/product_01.jpg" alt="Brenden Legros">
                            </div>
                            <h4>Keynote <span>Brenden Legros</span></h4>
                            <p>Facere provident incidunt quos voluptas.</p>
                            <a href="event.html" class="filled-button rounded-0">View Details</a>
                        </div>
                    </div>

                    <div class="row schedule-item">
                        <div class="col-md-2"><time>11:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>

                        <div class="col-md-10">
                            <div class="speaker">
                                <img src="assets/images/product_02.jpg" alt="Hubert Hirthe">
                            </div>
                            <h4>Et voluptatem iusto dicta nobis. <span>Hubert Hirthe</span></h4>
                            <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p>
                            <a href="event.html" class="filled-button rounded-0">View Details</a>
                        </div>
                    </div>

                    <div class="row schedule-item">
                        <div class="col-md-2"><time>12:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>

                        <div class="col-md-10">
                            <div class="speaker">
                                <img src="assets/images/product_03.jpg" alt="Cole Emmerich">
                            </div>
                            <h4>Explicabo et rerum quis et ut ea. <span>Cole Emmerich</span></h4>
                            <p>Veniam accusantium laborum nihil eos eaque accusantium aspernatur.</p>
                            <a href="event.html" class="filled-button rounded-0">View Details</a>
                        </div>
                    </div>

                </div>
                <!-- End Schdule Day 1 -->

                <!-- Schdule Day 2 -->
                <div role="tabpanel" class="col-lg-9  tab-pane fade" id="day-2">

                    <div class="row schedule-item">
                        <div class="col-md-2"><time>10:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>
                        <div class="col-md-10">
                            <div class="speaker">
                                <img src="assets/images/product_01.jpg" alt="Brenden Legros">
                            </div>
                            <h4>Keynote <span>Brenden Legros</span></h4>
                            <p>Facere provident incidunt quos voluptas.</p>
                            <a href="event.html" class="filled-button rounded-0">View Details</a>
                        </div>
                    </div>

                    <div class="row schedule-item">
                        <div class="col-md-2"><time>11:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>

                        <div class="col-md-10">
                            <div class="speaker">
                                <img src="assets/images/product_02.jpg" alt="Hubert Hirthe">
                            </div>
                            <h4>Et voluptatem iusto dicta nobis. <span>Hubert Hirthe</span></h4>
                            <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p>
                            <a href="event.html" class="filled-button rounded-0">View Details</a>
                        </div>
                    </div>

                    <div class="row schedule-item">
                        <div class="col-md-2"><time>12:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>

                        <div class="col-md-10">
                            <div class="speaker">
                                <img src="assets/images/product_03.jpg" alt="Cole Emmerich">
                            </div>
                            <h4>Explicabo et rerum quis et ut ea. <span>Cole Emmerich</span></h4>
                            <p>Veniam accusantium laborum nihil eos eaque accusantium aspernatur.</p>
                            <a href="event.html" class="filled-button rounded-0">View Details</a>
                        </div>
                    </div>

                    <div class="row schedule-item">
                        <div class="col-md-2"><time>10:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>
                        <div class="col-md-10">
                            <div class="speaker">
                                <img src="assets/images/product_01.jpg" alt="Brenden Legros">
                            </div>
                            <h4>Keynote <span>Brenden Legros</span></h4>
                            <p>Facere provident incidunt quos voluptas.</p>
                            <a href="event.html" class="filled-button rounded-0">View Details</a>
                        </div>
                    </div>

                    <div class="row schedule-item">
                        <div class="col-md-2"><time>11:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>

                        <div class="col-md-10">
                            <div class="speaker">
                                <img src="assets/images/product_02.jpg" alt="Hubert Hirthe">
                            </div>
                            <h4>Et voluptatem iusto dicta nobis. <span>Hubert Hirthe</span></h4>
                            <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p>
                            <a href="event.html" class="filled-button rounded-0">View Details</a>
                        </div>
                    </div>

                    <div class="row schedule-item">
                        <div class="col-md-2"><time>12:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>

                        <div class="col-md-10">
                            <div class="speaker">
                                <img src="assets/images/product_03.jpg" alt="Cole Emmerich">
                            </div>
                            <h4>Explicabo et rerum quis et ut ea. <span>Cole Emmerich</span></h4>
                            <p>Veniam accusantium laborum nihil eos eaque accusantium aspernatur.</p>
                            <a href="event.html" class="filled-button rounded-0">View Details</a>
                        </div>
                    </div>
                </div>
                <!-- End Schdule Day 2 -->

                <!-- Schdule Day 3 -->
                <div role="tabpanel" class="col-lg-9  tab-pane fade" id="day-3">

                    <div class="row schedule-item">
                        <div class="col-md-2"><time>10:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>
                        <div class="col-md-10">
                            <div class="speaker">
                                <img src="assets/images/product_01.jpg" alt="Brenden Legros">
                            </div>
                            <h4>Keynote <span>Brenden Legros</span></h4>
                            <p>Facere provident incidunt quos voluptas.</p>
                            <a href="event.html" class="filled-button rounded-0">View Details</a>
                        </div>
                    </div>

                    <div class="row schedule-item">
                        <div class="col-md-2"><time>11:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>

                        <div class="col-md-10">
                            <div class="speaker">
                                <img src="assets/images/product_02.jpg" alt="Hubert Hirthe">
                            </div>
                            <h4>Et voluptatem iusto dicta nobis. <span>Hubert Hirthe</span></h4>
                            <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p>
                            <a href="event.html" class="filled-button rounded-0">View Details</a>
                        </div>
                    </div>

                    <div class="row schedule-item">
                        <div class="col-md-2"><time>12:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>

                        <div class="col-md-10">
                            <div class="speaker">
                                <img src="assets/images/product_03.jpg" alt="Cole Emmerich">
                            </div>
                            <h4>Explicabo et rerum quis et ut ea. <span>Cole Emmerich</span></h4>
                            <p>Veniam accusantium laborum nihil eos eaque accusantium aspernatur.</p>
                            <a href="event.html" class="filled-button rounded-0">View Details</a>
                        </div>
                    </div>

                    <div class="row schedule-item">
                        <div class="col-md-2"><time>10:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>
                        <div class="col-md-10">
                            <div class="speaker">
                                <img src="assets/images/product_01.jpg" alt="Brenden Legros">
                            </div>
                            <h4>Keynote <span>Brenden Legros</span></h4>
                            <p>Facere provident incidunt quos voluptas.</p>
                            <a href="event.html" class="filled-button rounded-0">View Details</a>
                        </div>
                    </div>

                    <div class="row schedule-item">
                        <div class="col-md-2"><time>11:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>

                        <div class="col-md-10">
                            <div class="speaker">
                                <img src="assets/images/product_02.jpg" alt="Hubert Hirthe">
                            </div>
                            <h4>Et voluptatem iusto dicta nobis. <span>Hubert Hirthe</span></h4>
                            <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p>
                            <a href="event.html" class="filled-button rounded-0">View Details</a>
                        </div>
                    </div>

                    <div class="row schedule-item">
                        <div class="col-md-2"><time>12:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>

                        <div class="col-md-10">
                            <div class="speaker">
                                <img src="assets/images/product_03.jpg" alt="Cole Emmerich">
                            </div>
                            <h4>Explicabo et rerum quis et ut ea. <span>Cole Emmerich</span></h4>
                            <p>Veniam accusantium laborum nihil eos eaque accusantium aspernatur.</p>
                            <a href="event.html" class="filled-button rounded-0">View Details</a>
                        </div>
                    </div>

                </div>
                <!-- End Schdule Day 2 -->

            </div>

        </div>

    </section>
    <!-- End Schedule Section -->
    <!-- Events Details bar End Here -->
    <div class="latest-sports">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h2 class="text-white">Event Sports</h2>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="product-item">
                        <a href="#"><img src="assets/images/martialarts.png" alt=""></a>
                        <div class="down-content text-center">
                            <a href="#">
                                <h4 class="links-dark-red">View Events</h4>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="product-item">
                        <a href="#"><img src="assets/images/football.png" alt=""></a>
                        <div class="down-content text-center">
                            <a href="#">
                                <h4 class="links-dark-red">View Events</h4>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="product-item">
                        <a href="#"><img src="assets/images/cricket_PNG55.png" alt=""></a>
                        <div class="down-content text-center">
                            <a href="#">
                                <h4 class="links-dark-red">View Events</h4>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection