@extends('layouts.admin.site')
@section('content')
<h4 class="font-weight-bold py-3 mb-4">
    <span class="text-muted font-weight-light">User </span>
</h4>
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>{{ session('msg') }}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
 </div>
<!-- DataTable within card -->

<div class="card">
     @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<h6 class="card-header text-right">
    <a href="{{ route('users.create') }}" class="btn btn-success" > Add New</a>
</h6>
    <div class="card-datatable table-responsive">
        <table class="datatables-demo table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Active</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>

                @isset($users)
                    @if (count($users) > 0)
                        @foreach ($users as $user)
                        <tr class="odd gradeX">
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name  }}</td>
                            <td>{{ $user->email  }}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-success dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">Active</button>
                                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: top, left; top: 25px; left: 0px;">
                                        <a class="dropdown-item">Deactivate</a>
                                    </div>
                                </div>
                            </td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-dark dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">Action</button>
                                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: top, left; top: 35px; left: 0px;">
                                        <a href="{{ route('users.show',$user->id) }}" class="dropdown-item"><i class="fa fa-eye"></i> View Profile</a>

                                        {{--  <a href="" class="dropdown-item"><i class="fa fa-plus"></i> Add User </a>  --}}

                                        <a href="{{ route('users.edit',$user->id) }}" class="dropdown-item"><i class="fa fa-edit"></i> Edit </a>
                                        <form action="{{ route('users.destroy',$user->id) }}" class="d-flex" method="post">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="submit" class="dropdown-item">
                                                <i class="fa fa-trash"></i> Delete
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <div class="m-3 text-center col-12">
                            <p>No Organzier found...!</p>
                        </div>
                    @endif
                @endisset


            </tbody>
        </table>
    </div>
</div>
@endsection
