@extends("layouts.admin.site")
@section("content")

<div class="media align-items-center py-3 mb-3">
    <img src="assets/img/avatars/5-small.png" alt="" class="d-block ui-w-100 rounded-circle">
    <div class="media-body ml-4">
        <h4 class="font-weight-bold mb-0">{{ $user->name }}</h4>
        <div class="text-muted mb-2">ID: 3425433</div>
        <a href="{{ route('users.edit',$user->id) }}" class="btn btn-primary btn-sm">Edit</a>&nbsp;
        <a href="javascript:void(0)" class="btn btn-default">Back</a>&nbsp;
        {{--  <a href="javascript:void(0)" class="btn btn-default btn-sm icon-btn"><i class="ion ion-md-mail"></i></a>  --}}
    </div>
</div>


<div class="card">

    <div class="card-body">
        @isset($user)
        <table class="table user-view-table m-0">
            <tbody>
                <tr>
                    <td>Username:</td>
                    <td>{{ $user->user_name }}</td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td>{{ $user->name }}</td>
                </tr>
                <tr>
                    <td>E-mail:</td>
                    <td>{{ $user->email }}</td>
                </tr>
                <tr>
                    <td>Phone:</td>
                    <td>{{ $user->phone }}</td>
                </tr>
                <tr>
                    <td>Status:</td>
                    <td><span class="badge badge-outline-success">Active</span></td>
                </tr>
                <tr>
                    <td>Country:</td>
                    <td>{{ $user->country }}</td>
                </tr>
                <tr>
                    <td>City:</td>
                    <td>{{ $user->city }}</td>
                </tr>
                <tr>
                    <td>State:</td>
                    <td>{{ $user->state }}</td>
                </tr>
            </tbody>
        </table>

        @endisset
    </div>
</div>
@endsection
