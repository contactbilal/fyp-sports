@extends("layouts.admin.site")
@section("content")
<div class="media align-items-center py-3 mb-3">
    <img src="assets/img/avatars/5-small.png" alt="" class="d-block ui-w-100 rounded-circle">
    <div class="media-body ml-4">

        <a href="{{ route('news.edit', $news->id) }}" class="btn btn-primary btn-sm">Edit</a>&nbsp;
    </div>
</div>


<div class="card">

    <div class="card-body">

        <table class="table user-view-table m-0">
            <tbody>

                <tr>
                    <td>News title:</td>
                    <td>{{ $news->title }}</td>
                </tr>

                <tr>
                    <td>Description</td>
                    <td>{{ $news->description }}</td>
                </tr>

            </tbody>
        </table>

    </div>
</div>
@endsection
