@extends("layouts.admin.site")
@section("content")
<h4 class="font-weight-bold py-3 mb-4">
    Edit Event <span class="text-muted"></span>
</h4>

<div class="nav-tabs-top">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#user-edit-account">Account</a>
        </li>

    </ul>
    <div class="tab-content">
        <div class="tab-pane fade show active" id="user-edit-account">
            <form method="POST" action="{{route('events.update',$event->id)}}">
                @csrf
                @method('put')

                <div class="card-body">

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="card-body">
{{--
                    <div class="media align-items-center">
                        <img src="assets/img/avatars/5-small.png" alt="" class="d-block ui-w-80">
                        <div class="media-body ml-3">
                            <label class="form-label d-block mb-2">Avatar</label>
                            <label class="btn btn-outline-primary btn-sm">
      Add
      <input type="file" class="user-edit-fileinput">
    </label>&nbsp;

                        </div>
                    </div> --}}

                </div>
                <hr class="border-light m-0">
                <div class="card-body pb-2">

                    <div class="form-group">
                        <label class="form-label">Event Name</label>
                        <input type="text" name="event_name" class="form-control mb-1" value="{{ $event->event_name }}" placeholder="Event Name">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Event Category</label>
                        <select name="category_id" class="custom-select">
                          <option selected disabled hidden>{{ $event->category->category_name ?? '' }}</option>
                          @isset($categories)
                              @foreach ($categories as $category)
                                <option  value="{{ $category->id ?? '' }}">{{ $category->category_name ?? '' }}</option>
                              @endforeach
                          @endisset
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Location</label>
                        <input type="text" name="location" class="form-control mb-1" value="{{ $event->location }}" placeholder="Location">
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-label">Date</label>
                            <input type="date" name="date" value="{{ $event->date }}" class="form-control placeholder="Date">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-label">Time</label>
                            <input type="time" name="time" class="form-control" value="{{ $event->time }}" placeholder="Date">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="form-label">Description</label>
                        <textarea name="description" id="" class="form-control" cols="30" rows="10" >{{ $event->description }}</textarea>
                    </div>
                </div>
                <div class="text-right mt-3">
                    <button type="submit" class="btn btn-primary">Save changes</button>&nbsp;
                    {{-- <button type="button" class="btn btn-default">Cancel</button> --}}
                </div>
            </form>


        </div>
    </div>
</div>
@endsection
