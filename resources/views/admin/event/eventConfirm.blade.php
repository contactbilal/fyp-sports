@extends('layouts.admin.site')
@section('content')
<h4 class="font-weight-bold py-3 mb-4">
    <span class="text-muted font-weight-light">event </span>
</h4>
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>{{ session('msg') }}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
 </div>
<!-- DataTable within card -->

<div class="card">
    <h6 class="card-header">
        <h6 class="card-header text-right">
            <a href="{{ route('events.create') }}" class="btn btn-success" > Add New</a>
        </h6>
    </h6>
    <div class="card-datatable table-responsive">
        <table class="datatables-demo table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Event Name</th>
                    <th>Location</th>
                    <th>Date</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>

                @isset($events)
                    @if (count($events) > 0)
                        @foreach ($events as $event)
                        <tr class="odd gradeX">
                            <td>{{ $event->id ?? '' }}</td>
                            <td>{{ $event->event_name ?? '' }}</td>
                            <td>{{ $event->category->category_name ?? '' }}</td>
                            <td>{{ $event->date ?? '' }} {{ $event->time ?? '' }}</td>
                            <td>{{ $event->location ?? '' }}</td>
                            <td>{{ $event->description ?? '' }}</td>

                            <td class="text-center">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-success dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">Done</button>
                                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: top, left; top: 25px; left: 0px;">
                                        <a class="dropdown-item">undone</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <div class="m-3 text-center col-12">
                            <p>No Players found...!</p>
                        </div>
                    @endif
                @endisset


            </tbody>
        </table>
    </div>
</div>
@endsection
