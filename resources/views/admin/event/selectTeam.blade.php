@extends('layouts.admin.site')
@section('content')
<h4 class="font-weight-bold py-3 mb-4">
    <span class="text-muted font-weight-light">Teams </span>
</h4>
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>{{ session('msg') }}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
 </div>
<!-- DataTable within card -->

<div class="card">

        <h6 class="card-header text-right">
            <a href="#" class="btn btn-success" > Add New team</a>
        </h6>

    <div class="all team">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('event.add.team') }}" method="post">

            @csrf

            <input type="hidden" name="event_id" value="{{ $event->id ?? ''}}">

            @isset($teams)
                @if (count($teams) > 0)
                    @foreach ($teams as $team)
                        <div class="form-check border-bottom p-3 pl-5">
                            <input class="form-check-input" name="team[]" type="checkbox" value="{{ $team->id ?? '' }}">
                            <label class="form-check-label" for="defaultCheck1">
                                {{ $team->team_name ?? '' }}
                            </label>
                        </div>
                    @endforeach
                @else
                    <p>No team Found</p>
                @endif
            @endisset

            <div class="text-right mt-3 mb-3 mr-3">
                <button type="submit" class="btn btn-primary">Add Team</button>&nbsp;
                {{-- <button type="button" class="btn btn-default">Cancel</button> --}}
            </div>

    </form>

    </div>
</div>
@endsection
