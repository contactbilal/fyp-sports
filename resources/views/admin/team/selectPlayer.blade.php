@extends('layouts.admin.site')
@section('content')
<h4 class="font-weight-bold py-3 mb-4">
    <span class="text-muted font-weight-light">Teams </span>
</h4>
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>{{ session('msg') }}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
 </div>
<!-- DataTable within card -->

<div class="card">

        <h6 class="card-header text-right">
            <a href="#" class="btn btn-success" > Add New player</a>
        </h6>

    <div class="all player">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('team.add.player') }}" method="post">

            @csrf

            <input type="hidden" name="team_id" value="{{ $team->id ?? ''}}">

            @isset($players)
                @if (count($players) > 0)
                    @foreach ($players as $player)
                        <div class="form-check border-bottom p-3 pl-5">
                            <input class="form-check-input" name="player[]" type="checkbox" value="{{ $player->id ?? '' }}">
                            <label class="form-check-label" for="defaultCheck1">
                                {{ $player->player_name ?? '' }}
                            </label>
                        </div>
                    @endforeach
                @else
                    <p>No Player Found</p>
                @endif
            @endisset

            <div class="text-right mt-3 mb-3 mr-3">
                <button type="submit" class="btn btn-primary">Add player</button>&nbsp;
                {{-- <button type="button" class="btn btn-default">Cancel</button> --}}
            </div>

    </form>

    </div>
</div>
@endsection
