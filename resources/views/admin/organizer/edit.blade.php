@extends("layouts.admin.site")
@section("content")
    <h4 class="font-weight-bold py-3 mb-4">
        Edit Organizer <span class="text-muted"></span>
    </h4>

    <div class="nav-tabs-top">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#user-edit-account">Account</a>
            </li>

        </ul>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="user-edit-account">
                <form method="POST" action="{{route('organizers.update',$organizer->id)}}">
                    @csrf
                    @method('put')

                    <div class="card-body">

                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                        <div class="media align-items-center">
                            <img src="assets/img/avatars/5-small.png" alt="" class="d-block ui-w-80">
                            <div class="media-body ml-3">
                                <label class="form-label d-block mb-2">Avatar</label>
                                <label class="btn btn-outline-primary btn-sm">
        Change
        <input type="file" class="user-edit-fileinput">
        </label>&nbsp;
                                <button type="button" class="btn btn-default btn-sm md-btn-flat">Reset</button>
                            </div>
                        </div>

                    </div>
                    <hr class="border-light m-0">
                    <div class="card-body pb-2">

                        <div class="form-group">
                            <label class="form-label">Username</label>
                            <input type="text" class="form-control mb-1" name="user_name" placeholder="User Name" value="{{ $organizer->user_name }}">
                        </div>

                        <div class="form-group">
                            <label class="form-label">Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Name" value="{{ $organizer->name }}">
                        </div>
                        <div class="form-group">
                            <label class="form-label">E-mail</label>
                            <input type="text" class="form-control mb-1" name="email" placeholder="example@exe.com" value="{{ $organizer->email }}">
                            {{-- <a href="javascript:void(0)" class="small">Resend confirmation</a> --}}
                        </div>
                        <div class="form-group">
                            <label class="form-label">Phone</label>
                            <input type="text" class="form-control" name="phone" placeholder="Phone" value="{{ $organizer->phone }}">
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-label">Country</label>
                                    <input type="text" class="form-control" name="country" placeholder="City" value="{{ $organizer->country }}">
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-label">City</label>
                                    <input type="text" class="form-control" name="city" placeholder="City" value="{{ $organizer->city }}">
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-label">State</label>
                                    <input type="text" class="form-control" name="state" placeholder="state" value="{{ $organizer->state }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-right m-3 px-3">
                        <button type="submit" class="btn btn-primary">Save changes</button>&nbsp;
                        <a href="{{ route('organizers.index') }}" class="btn btn-default">Back</a>
                    </div>
                </form>


            </div>
        </div>
    </div>

@endsection
