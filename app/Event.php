<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'organizer_id','category_id','event_name','avtar','date','time','location','description'
    ];

    public function category()
    {
        return $this->hasOne(Categories::class, 'id', 'category_id');
    }

}
