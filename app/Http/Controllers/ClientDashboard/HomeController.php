<?php

namespace App\Http\Controllers\ClientDashboard;

use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    public function __invoke(){
        return view('website.clientDashboard.index');
    }


}
