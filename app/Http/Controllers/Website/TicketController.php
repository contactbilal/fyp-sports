<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Event;
use App\UserEventTicket;
use Session;
use Auth;

use Stripe;

class TicketController extends Controller
{
    //
    public function ticket($event_id){

        $event = Event::where('id', $event_id)->first();
        return view('website.ticket', compact('event'));

    }

    public function ticketPost(Request $request)

    {   

        $eventId = $request->event_id; 

        $myTicket = new UserEventTicket;
        $myTicket->user_id = Auth::user()->id;
        $myTicket->event_id = $eventId;
        $myTicket->price = $request->amount;
        $myTicket->booking = 'Seat Confirm';
        $myTicket->save();


        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        Stripe\Charge::create ([

                "amount" => $request->amount,

                "currency" => "usd",

                "source" => $request->stripeToken,

                "description" => "This is test Payment." 

        ]);

  

        Session::flash('success', 'Payment successful!');

        return redirect('/');

    }


}
