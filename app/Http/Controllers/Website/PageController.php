<?php

namespace App\Http\Controllers\Website;
use App\Models\ContentPage;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Event;
use App\Categories;
use App\EventHaveTeam;
use App\Team;
use App\News;

use Carbon\Carbon;


class PageController extends Controller
{
    public function __invoke($slug){
        // event details
        $totalEvent = Event::count();
        $currentEvent=Event::whereDate('date', Carbon::today())->orderBy('created_at','asc')->count();
        $totalUpcomingEvents=Event::whereDate('date', Carbon::now()->add(2, 'day'))->count();

        //Latest 6 Record of event

        $latestEvents=Event::latest()->take(6)->get();

        //Latest   all and 6 Record of Newss

        $allNewsShow=News::latest()->get();
        $latestNews=News::latest()->take(6)->get();

        // latest categories
        $allCategories = Categories::latest()->get();

// dd( $allCategories);
        if($slug ==  'home'){
            return view('website.pages.home',compact('totalEvent','latestNews','currentEvent','totalUpcomingEvents','latestEvents','allCategories'));
        } elseif($slug == 'about'){
            return view('website.pages.about');
        }
        elseif($slug == 'eventList'){

            $allCategories = Categories::latest()->get();
            $events = Event::latest()->paginate(10);

            return view('website.pages.eventList', compact('events', 'allCategories'));

        }
        elseif($slug == 'news'){
            return view('website.pages.news',compact('allNewsShow'));
        }
        elseif($slug == 'schedule'){
            return view('website.pages.schedule');
        }
        elseif($slug == 'contact'){
            return view('website.pages.contact');
        }
        elseif($slug == 'login'){
            return view('website.pages.login');
        }
        elseif($slug == 'signup'){
            return view('website.pages.signup');
        }

    }

    public static function eventCategoryPage($id){

        $event=Event::find($id);
        $categoryId=$event->category_id;

        $categories = Categories::find($categoryId);

        if($categories->slug=="ufc"){
            return view('website.eventTemplates.ufc');
        }else if($categories->slug=="cricket"){
            return view('website.eventTemplates.cricket');
        }elseif($categories->slug=="football"){
            return view('website.eventTemplates.football');
        }
    }

    public function eventByCategory($slug){

        $category = Categories::where('slug', $slug)->first();
        $events = Event::where('category_id', $category->id)->get();

        return view('website.category.index', compact('events', 'category'));


    }

    public function eventDetail($id){

        $event=Event::where('id', $id)->first();
        $remaining_seats = $event->seats - $event->confirm_seats;

        $eventTeamsData = EventHaveTeam::where("event_id", $event->id)->get()->pluck("team_id")->toArray();
        $eventTeams = Team::whereIn('id', $eventTeamsData)->latest()->get();

        return view('website.eventTemplates.index', compact('event', 'remaining_seats', 'eventTeams'));

    }

    public static function showNews($id){

        $news=News::find($id);
        return view('website.pages.newsdetail',compact('news'));

    }


}
