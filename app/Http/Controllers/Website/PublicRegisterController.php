<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\Redirect;

class PublicRegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

        $validateData = $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'user_name' => 'required|unique:users',
            'phone' => 'required',
            'country' => 'required',
            'city' => 'required',
            'state' => 'required',
            'role' => 'required'
          ]);

          $user = new User;
          $user->name = $request->name;
          $user->email  = $request->email ;
          $user->password = bcrypt($request->password);
          $user->user_name = $request->user_name;
          $user->phone = $request->phone;
          $user->country = $request->country;
          $user->city = $request->city;
          $user->state = $request->state;
          $user->save();

          $user->assignRole($request->role);

          $credentials = $request->only('email', 'password');

          if (Auth::attempt($credentials)) {
            // Authentication passed...

            if(Auth::user()->hasRole('user')){

                // return redirect()->route('userdashboard');
                return redirect()->route('userdashboard.home');

              }elseif(Auth::user()->hasRole('organizer')){

                return 'I am Organizer';


              }


        }






    }
}
