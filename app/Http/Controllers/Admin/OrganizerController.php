<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\User;

class OrganizerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $organizers = User::role('organizer')->get();
        return view('admin.organizer.index', compact('organizers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.organizer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'user_name' => 'required|unique:users',
            'phone' => 'required',
            'country' => 'required',
            'city' => 'required',
            'state' => 'required',
          ]);

        $user = new User;
        $user->name = $request->name;
        $user->email  = $request->email ;
        $user->password = bcrypt($request->password);
        $user->user_name = $request->user_name;
        $user->phone = $request->phone;
        $user->country = $request->country;
        $user->city = $request->city;
        $user->state = $request->state;
        $user->save();

        $user->assignRole('organizer');
        $request->session()->flash('msg','Record successfully inserted');
        return redirect()->route('organizers.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $organizer=User::find($id);
        return view('admin.organizer.show',compact('organizer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $organizer=User::find($id);
        return view('admin.organizer.edit',['organizer'=>$organizer]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        $validateData = $request->validate([

            'email' => ['required',
                Rule::unique('users')->ignore($id)],
            'user_name' => ['required',
                Rule::unique('users')->ignore($id)],
            'name' => 'required',
            'phone' => 'required',
            'country' => 'required',
            'city' => 'required',
            'state' => 'required',
          ]);

        $user=  User::find($id);

        $user->name = $request->name;
        $user->email  = $request->email ;
        $user->user_name = $request->user_name;
        $user->phone = $request->phone;
        $user->country = $request->country;
        $user->city = $request->city;
        $user->state = $request->state;
        $user->save();


        // message
        $request->session()->flash('msg','Record Updated');
        // redirect to home
        return redirect()->route('organizers.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $user=  User::find($id);
        // $user->delete();
        $data =DB::table('users')
        ->leftJoin('model_has_roles','users.id', '=','model_has_roles.model_id')
        ->where('users.id', $id);
DB::table('model_has_roles')->where('model_id', $id)->delete();

$data->delete();
    // message
    session()->flash('msg','Record Deleted');
    // redirect to home
    return redirect()->route('organizers.index');

         // message
         session()->flash('msg','Record Deleted');
         // redirect to home
         return redirect()->route('organizers.index');
    }
}
