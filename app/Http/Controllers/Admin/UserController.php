<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Services\FileUploadService;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::role('user')->get();
        return view('admin.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'user_name' => 'required|unique:users',
            'phone' => 'required',
            'country' => 'required',
            'city' => 'required',
            'state' => 'required',
          ]);
        $file = FileUploadService::upload( $request->file('avatar'), 'public/User' );
        $user = new User;
        $user->name = $request->name;
        $user->avatar =$request->file->uploaded_name;
        $user->email  = $request->email ;
        $user->password = bcrypt($request->password);
        $user->user_name = $request->user_name;
        $user->phone = $request->phone;
        $user->country = $request->country;
        $user->city = $request->city;
        $user->state = $request->state;
        $user->save();

        $user->assignRole('data-entry');
        $request->session()->flash('msg','Record successfully inserted');
        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('admin.user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user=User::find($id);
        return view('admin.user.edit',['user'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if ($request->hasFile('avatar') && $request->file('avatar')) {
            $validateData = $request->validate([

                'email' => ['required',
                    Rule::unique('users')->ignore($id)],
                'user_name' => ['required',
                    Rule::unique('users')->ignore($id)],
                'name' => 'required',
                'phone' => 'required',
                'country' => 'required',
                'city' => 'required',
                'state' => 'required',
              ]);
              $file = FileUploadService::upload( $request->file('avatar'), 'public/User' );
              $user=  User::find($id);
              $user->name = $request->name;
              $user->avatar =$request->file->uploaded_name;
              $user->email  = $request->email ;
              $user->user_name = $request->user_name;
              $user->phone = $request->phone;
              $user->country = $request->country;
              $user->city = $request->city;
              $user->state = $request->state;
              $user->save();

              $request->session()->flash('msg','Record Updated');
              // redirect to home
              return redirect()->route('users.index');

        }else{
            $validateData = $request->validate([

                'email' => ['required',
                    Rule::unique('users')->ignore($id)],
                'user_name' => ['required',
                    Rule::unique('users')->ignore($id)],
                'name' => 'required',
                'phone' => 'required',
                'country' => 'required',
                'city' => 'required',
                'state' => 'required',
              ]);


              $user=  User::find($id);
              $user->name = $request->name;
              $user->avatar =$request->file->uploaded_name;
              $user->email  = $request->email ;
              $user->password = bcrypt($request->password);
              $user->user_name = $request->user_name;
              $user->phone = $request->phone;
              $user->country = $request->country;
              $user->city = $request->city;
              $user->state = $request->state;
              $user->save();

              $request->session()->flash('msg','Record Updated');
              // redirect to home
              return redirect()->route('users.index');
        }





        // message

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User::find($id);
        $user->roles()->detach();
        if ($user != null){
            $user->delete();
            session()->flash('msg','User Deleted Successfully');
        }

        return back();
    }
}
