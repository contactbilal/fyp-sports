<?php

namespace App\Http\Controllers\Admin;
use App\Services\FileUploadService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\News;
use Auth;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allNews =News::latest()->get();
        return view('admin.news.index',compact('allNews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'title' => 'required',
            'description' => 'required',
         ]);

         $file = FileUploadService::upload( $request->file('avatar'), 'public/news' );
         $allNews = new News;
         $allNews->organizer_id = Auth::user()->id;
         $allNews->title= $request->title;
         $allNews->description= $request->description;
         $allNews->avatar =$file->uploaded_name;
         $allNews->save();

         return redirect()->route('news.index');
         $request->session()->flash('msg','Record successfully inserted');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news=News::find($id);
        return view('admin.news.show',compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news=News::find($id);
        return view('admin.news.edit',['news'=>$news]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if ($request->hasFile('avatar') && $request->file('avatar')) {
            $validateData = $request->validate([
                'title' => 'required',
                'description' => 'required',
             ]);

             $file = FileUploadService::upload( $request->file('avtar'), 'public/team' );

             $news =News::find($id);
             $news->title= $request->title;
             $news->description= $request->description;
            $news->avatar =$file->uploaded_name;
             $news->save();
             return  redirect()->route('teams.index');
        }else{
            $validateData = $request->validate([
                'title' => 'required',
                'description' => 'required',
             ]);


             $news =News::find($id);


             $news->organizer_id = Auth::user()->id;
             $news->title= $request->title;
             $news->description= $request->description;
             $news->avatar ='dummy';
             $news->save();

             return redirect()->route('news.index');

             $request->session()->flash('msg','Record successfully inserted');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team=News::find($id);
        $team->delete();
        if ($team != null){
            $team->delete();
            session()->flash('msg','User Deleted Successfully');
        }
    }
}
