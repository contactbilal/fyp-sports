<?php

namespace App\Http\Controllers;

use App\TeamHavePlaye;
use Illuminate\Http\Request;

class TeamHavePlayeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TeamHavePlaye  $teamHavePlaye
     * @return \Illuminate\Http\Response
     */
    public function show(TeamHavePlaye $teamHavePlaye)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TeamHavePlaye  $teamHavePlaye
     * @return \Illuminate\Http\Response
     */
    public function edit(TeamHavePlaye $teamHavePlaye)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TeamHavePlaye  $teamHavePlaye
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TeamHavePlaye $teamHavePlaye)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TeamHavePlaye  $teamHavePlaye
     * @return \Illuminate\Http\Response
     */
    public function destroy(TeamHavePlaye $teamHavePlaye)
    {
        //
    }
}
