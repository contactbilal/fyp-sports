<?php


namespace App\Services;

use Exception;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

use function md5;
use function microtime;

class FileUploadService
{
    public static function upload(UploadedFile $file,$path = 'files')
    {
        try{

            $name = md5(microtime()).'.'.$file->getClientOriginalExtension();

            $file->storeAs($path,$name);
            $file->uploaded_name = $name;
            $file->uploaded_path = $path;
            return $file;

        }catch(Exception $exception){

            session()->flash('alert-danger','Something wrong with Images Data');
            return null;

        }
    }

    public static function delete($filename, $path = 'files/')
    {
        try{

            if ( Storage::exists($path.$filename) ){
                Storage::delete($path.$filename);
            }

            return true;

        }catch(Exception $exception){

            session()->flash('alert-danger','Something wrong with File System');
            return null;

        }
    }
}
