<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = [
    'avtar','team_name','email','phone','country','city','state'
    ];
}
